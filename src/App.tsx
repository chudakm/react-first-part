import React from "react";
import { Chat } from "./components";

export const App = () => {
  return (
    <div>
      <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />
    </div>
  );
};
