import { IApiMessage } from "./types";

export class MessageApi {
  public static async get(url: string): Promise<IApiMessage[]> {
    const response = await fetch(url);
    const body = await response.json();
    return body;
  }
}
