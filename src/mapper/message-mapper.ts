import { IApiMessage } from "../api/message/types";
import { IMessage } from "../types";

export class MessageMapper {
  static fromApiMessages(messages: IApiMessage[]): IMessage[] {
    return messages.map(message => {
      return {
        id: message.id,
        userId: message.userId,
        avatar: message.avatar,
        username: message.user,
        text: message.text,
        createdAt: new Date(message.createdAt),
        editedAt: message.editedAt ? new Date(message.editedAt) : undefined,
        isLiked: false,
        ownMessage: false,
      };
    });
  }
}
