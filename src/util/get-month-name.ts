export const getMonthName = (month: number) => {
  return ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][month];
};
