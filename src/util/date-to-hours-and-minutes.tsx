export const dateToHoursAndMinutes = (date: Date) => {
  return `${date.getHours()}:${date.getMinutes()}`;
};
