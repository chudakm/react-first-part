import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { MessageApi } from "../../api";
import { MessageMapper } from "../../mapper/message-mapper";
import { IMessage } from "../../types";
import "./chat.scss";
import { Header, MessageList, MessageInput } from "./components";
import { Preloader } from "./components/preloader";
import { v4 as uuidv4 } from "uuid";

interface IChatProps {
  url: string;
}

export const Chat: React.FC<IChatProps> = ({ url }) => {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [isMessagesLoading, setIsMessagesLoading] = useState(true);
  const [editingMessageId, setEditingMessageId] = useState<null | string>(null);
  const [text, setText] = useState<string>("");

  useEffect(() => {
    MessageApi.get(url).then(apiMessages => {
      const messages = MessageMapper.fromApiMessages(apiMessages);
      setMessages(messages);
      setIsMessagesLoading(false);
    });
  }, [url]);

  const getParticipantsCount = () => {
    const userIds = messages.map(({ userId }) => userId);
    return new Set(userIds).size;
  };

  const onLike = (messageId: string) => {
    const copiedMessages = [...messages];
    const message = copiedMessages.find(({ id }) => id === messageId);
    if (!message) return;

    message.isLiked = !message.isLiked;
    setMessages(copiedMessages);
  };

  if (isMessagesLoading) {
    return <Preloader />;
  }

  const onSend = () => {
    if (editingMessageId) {
      const message = messages.find(({ id }) => id === editingMessageId);
      if (!message) return;
      message.text = text;

      setMessages([...messages]);
      setEditingMessageId(null);
    } else {
      const message: IMessage = {
        id: uuidv4(),
        userId: "12245",
        text,
        ownMessage: true,
        isLiked: false,
        createdAt: new Date(),
        avatar: "",
        username: "Maxim",
      };

      setMessages([...messages, message]);
    }

    setText("");
  };

  const onEdit = (id: string, text: string) => {
    setEditingMessageId(id);
    setText(text);
  };

  const onDelete = (id: string) => {
    setMessages([...messages].filter(message => message.id !== id));
  };

  return (
    <div className='chat'>
      <Header
        chatName='BSA Chat'
        participantsCount={getParticipantsCount()}
        messagesCount={messages.length}
        lastMessageAt={messages.length ? messages[messages.length - 1].createdAt : new Date()}
      />
      <br />
      <MessageList messages={messages} onLike={onLike} onEdit={onEdit} onDelete={onDelete} />
      <br />
      <MessageInput text={text} isEditing={!!editingMessageId} onInput={setText} onSend={onSend} />
    </div>
  );
};
