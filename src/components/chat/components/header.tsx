import React from "react";
import "../chat.scss";

interface IHeaderProps {
  chatName: string;
  participantsCount: number;
  messagesCount: number;
  lastMessageAt: Date;
}

export const Header: React.FC<IHeaderProps> = ({ chatName, participantsCount, messagesCount, lastMessageAt }) => {
  return (
    <div className='header'>
      <div className='header-info'>
        <span className='header-title'>{chatName}</span>
        <span className='header-users-count'>{participantsCount}</span>
        <span className='header-messages-count'>{messagesCount}</span>
      </div>

      <span className='header-last-message-date'>{lastMessageTime(lastMessageAt)}</span>
    </div>
  );
};

const lastMessageTime = (lastMessageAt: Date) => {
  const day = lastMessageAt.getDate();
  const month = lastMessageAt.getMonth();
  const year = lastMessageAt.getFullYear();

  const hour = lastMessageAt.getHours();
  const minutes = lastMessageAt.getMinutes();

  return `${day < 10 ? "0" : ""}${day}.${month < 10 ? "0" : ""}${month + 1}.${year} ${hour}:${minutes < 10 ? "0" : ""}${minutes}`;
};
