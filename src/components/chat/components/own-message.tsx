import React from "react";
import "../chat.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { dateToHoursAndMinutes } from "../../../util/date-to-hours-and-minutes";

interface IOwnMessageProps {
  id: string;
  createdAt: Date;
  text: string;
  onEdit: (id: string, text: string) => void;
  onDelete: (id: string) => void;
}

export const OwnMessage: React.FC<IOwnMessageProps> = ({ id, createdAt, text, onEdit, onDelete }) => {
  const editMessage = () => {
    onEdit(id, text);
  };

  const deleteMessage = () => {
    onDelete(id);
  };

  return (
    <div className='own-message'>
      <span className='message-time'>{dateToHoursAndMinutes(createdAt)}</span>
      <FontAwesomeIcon className='message-edit' icon={faEdit} onClick={editMessage}></FontAwesomeIcon>
      <FontAwesomeIcon className='message-delete' icon={faTrash} onClick={deleteMessage}></FontAwesomeIcon>
      <br />

      <span className='message-text'>{text}</span>
    </div>
  );
};
