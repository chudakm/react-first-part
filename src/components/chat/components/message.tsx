import React from "react";
import classNames from "classnames";
import "../chat.scss";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { dateToHoursAndMinutes } from "../../../util/date-to-hours-and-minutes";

interface IMessageProps {
  id: string;
  text: string;
  createdAt: Date;
  isLiked: boolean;
  username: string;
  avatar: string;
  onLike: (id: string) => void;
}

export const Message: React.FC<IMessageProps> = ({ id, text, createdAt, isLiked, username, avatar, onLike }) => {
  const onHeartClick = () => onLike(id);

  return (
    <div className='message'>
      <div className='message-left'>
        <div className='message-user-avatar'>
          <img src={avatar} alt='avatar' />
        </div>
      </div>

      <div className='message-right'>
        <span className='message-user-name'>{username}</span>
        <span className='message-time'>{dateToHoursAndMinutes(createdAt)}</span>
        <FontAwesomeIcon
          className={classNames("like", { "message-like": !isLiked, "message-liked": isLiked })}
          onClick={onHeartClick}
          icon={faHeart}
        />

        <br />

        <span className='message-text'>{text}</span>
      </div>
    </div>
  );
};
