import React from "react";
import "../chat.scss";
import { IGroupedMessages, IMessage } from "../../../types";
import { getDayName } from "../../../util/get-day-name";
import { getMonthName } from "../../../util/get-month-name";
import { Message } from "./message";
import { OwnMessage } from "./own-message";

interface IMessageListProps {
  messages: IMessage[];
  onLike: (id: string) => void;
  onEdit: (id: string, text: string) => void;
  onDelete: (id: string) => void;
}

export const MessageList: React.FC<IMessageListProps> = ({ messages, onLike, onEdit, onDelete }) => {
  const dividedMessages = divideMessagesByGroups(messages);

  return (
    <div className='message-list'>
      {Object.keys(dividedMessages).map(groupName => {
        const messages = dividedMessages[groupName].map(message => {
          return (
            <React.Fragment key={message.id}>
              <br />
              {message.ownMessage ? (
                <OwnMessage id={message.id} text={message.text} createdAt={message.createdAt} onEdit={onEdit} onDelete={onDelete} />
              ) : (
                <Message
                  key={message.id}
                  id={message.id}
                  text={message.text}
                  createdAt={message.createdAt}
                  isLiked={message.isLiked}
                  username={message.username}
                  avatar={message.avatar}
                  onLike={onLike}
                />
              )}
            </React.Fragment>
          );
        });

        messages.unshift(<Divider key={groupName} name={groupName} />);

        return messages;
      })}
    </div>
  );
};

const Divider: React.FC<{ name: string }> = ({ name }) => {
  return <div className='messages-divider'>{name}</div>;
};

const divideMessagesByGroups = (messages: IMessage[]): IGroupedMessages => {
  const groups: IGroupedMessages = {};

  const getMessageGroup = (createdAt: Date) => {
    const nowDay = new Date().getDate();
    const createdAtDay = createdAt.getDate();

    switch (createdAtDay) {
      case nowDay:
        return "Today";
      case nowDay - 1:
        return "Yesterday";
      default: {
        const weekDay = createdAt.getDay();
        const month = createdAt.getMonth();
        const dayName = getDayName(weekDay);
        const monthName = getMonthName(month);

        return `${dayName}, ${createdAtDay} ${monthName}`;
      }
    }
  };

  messages.forEach(message => {
    const group = getMessageGroup(message.createdAt);
    if (groups[group]) groups[group].push(message);
    else groups[group] = [message];
  });

  return groups;
};
