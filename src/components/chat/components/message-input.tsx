import React from "react";
import "../chat.scss";

interface IMessageInputProps {
  text: string;
  isEditing: boolean;
  onInput: (text: string) => void;
  onSend: () => void;
}

export const MessageInput: React.FC<IMessageInputProps> = ({ text, isEditing, onInput, onSend }) => {
  const onMessageInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    onInput(e.target.value);
  };

  const sendMessage = () => {
    if (!text) return;
    onSend();
  };

  const onKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter") sendMessage();
  };

  return (
    <div className='message-input'>
      <textarea className='message-input-text' cols={100} rows={5} onInput={onMessageInput} value={text} onKeyPress={onKeyPress}></textarea>

      <button className='message-input-button' onClick={sendMessage}>
        {isEditing ? "Save" : "Send"}
      </button>
    </div>
  );
};
